extends CanvasLayer



func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func change_stage(stage_path):
	#fade to black
	get_node("anim").play("fade_in")
	yield(get_node("anim"), "finished")
	
	#change stage
	get_tree().change_scene(stage_path)
	
	#fade from black
	get_node("anim").play("fade_out")
	
