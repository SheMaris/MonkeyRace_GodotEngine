extends Timer

var spawn_items = [
	preload("res://scenes/rock.tscn"),
	preload("res://scenes/snake10.tscn"),
	preload("res://scenes/stick1.tscn"),
	preload("res://scenes/bird.tscn"),
	preload("res://scenes/vine_lucky.tscn"),
]

var num_banana = [ 1, 2, 3 ]
var pos_x = [132, 212, 280, 353, 420]

#var obs_id = [2,2,4,2,0,0,1,0,2,2,4,0,0,4,3,0,1,2,0,2,3,3,0,1,0,1,1,3,0,3,2,0,4,1,2,0,4,1,4,2,4,1,2,4,2,4,2,2,2,2]
#var obs_pos = [3,3,2,0,2,1,2,0,3,2,2,2,3,3,2,3,4,0,0,4,0,0,0,3,0,0,0,0,3,0,4,0,2,2,3,0,3,3,1,1,0,3,2,1,2,2,3,2,3,3]


var count=0

func _ready():
	connect("timeout",self,"_on_timeout")

func _on_timeout():
	
	var item = spawn_items[global.obs_id[count]].instance()
	var p = global.obs_pos[count]
	print(item.get_name())
	if (item.get_name()=="vine_lucky"):
		item.set_pos(Vector2(pos_x[p], -700))
	elif (item.get_name()=="bird"):
		if (global.obs_pos[count]<=2):
			item.set_side(0)
		else:
			item.set_side(1)
	else:
		item.set_pos(Vector2(pos_x[p], -550))
	get_parent().add_child(item)
	
	count+=1