extends Node2D

var count=1
onready var server = preload('../scripts/server.gd')
var music
func _ready():
	music=get_node("music")
	OS.set_window_size(Vector2(480,800))
	music.set_volume(info.getVolume())
	music.play()
	set_process(true)

func _process(dt):
	if (global.conn!=null):
		global.conn.is_listening()
	
func _on_ping(ping):
	print("Ping: ",ping)

func _on_error(err):
	print("Error: ",err)
	
func _on_connected():
	print("Connected to server")
	
func _on_start(data):
	mapa(data.random_num)
	var packed_scene = load("res://scenes/game.tscn")
	get_tree().change_scene_to(packed_scene)

func turn_on_connection():
	if (global.conn!=null):
		global.conn.connect("error",self,"_on_error")
		global.conn.connect("connected",self,"_on_connected")
		global.conn.connect("message",self,"_on_receive")
		global.conn.connect("ping",self,"_on_ping")
		global.conn.connect("start",self,"_on_start")

func _on_newGame_pressed():
	global.conn=server.new()
	turn_on_connection()
	if (global.conn!=null and count%2==0):
		global.conn.ping()
	#stage_manager.change_stage("res://scenes/game.tscn")

func _on_configuration_pressed():
	music.stop()
	stage_manager.change_stage("res://scenes/configuracion.tscn")

func _on_credits_pressed():
	music.stop()
	stage_manager.change_stage("res://scenes/credits.tscn")

func _on_exit_pressed():
	get_tree().quit()

func mapa(num):
	if(num==0):
		global.ban_id = [3, 3, 3, 2, 1, 3, 3, 2, 2, 2, 3, 2, 1, 1, 2, 1, 2, 1, 3, 3, 2, 1, 3, 3, 2, 2, 1, 1, 1, 1]
		global.ban_pos = [1, 4, 3, 3, 4, 0, 2, 2, 1, 2, 0, 2, 0, 3, 2, 4, 0, 0, 4, 3, 3, 4, 1, 1, 3, 1, 0, 2, 1, 4]
		global.obs_id = [0, 4, 2, 0, 3, 1, 0, 0, 1, 4, 2, 3, 3, 1, 4, 1, 1, 2, 4, 2, 4, 0, 3, 3, 1, 4, 3, 2, 0, 2]
		global.obs_pos = [4, 2, 0, 3, 1, 0, 2, 1, 1, 2, 3, 4, 4, 3, 4, 1, 1, 2, 3, 0, 2, 3, 4, 2, 3, 0, 4, 0, 0, 1]
	if(num==1):
		global.ban_id = [2, 2, 1, 3, 1, 3, 2, 2, 3, 3, 1, 2, 1, 1, 2, 1, 2, 3, 1, 2, 1, 1, 3, 3, 2, 3, 3, 2, 3, 1]
		global.ban_pos = [0, 3, 1, 1, 2, 4, 4, 0, 0, 4, 4, 2, 3, 1, 3, 0, 2, 0, 4, 3, 0, 3, 2, 2, 1, 1, 1, 2, 3, 4]
		global.obs_id = [3, 4, 3, 4, 2, 0, 0, 3, 4, 4, 3, 2, 2, 4, 4, 0, 2, 1, 1, 1, 0, 1, 1, 3, 3, 0, 0, 1, 2, 2]
		global.obs_pos = [0, 2, 2, 2, 1, 4, 0, 0, 1, 3, 4, 1, 1, 2, 4, 3, 0, 4, 0, 3, 4, 2, 4, 0, 1, 3, 3, 3, 2, 1]
	if(num==2):
		global.ban_id = [2, 1, 1, 2, 2, 1, 3, 1, 1, 3, 2, 1, 2, 3, 2, 2, 3, 1, 2, 2, 3, 1, 3, 1, 3, 3, 2, 3, 1, 3]
		global.ban_pos = [1, 0, 0, 2, 0, 3, 3, 0, 0, 2, 1, 4, 3, 1, 3, 0, 2, 4, 3, 4, 2, 2, 4, 4, 3, 1, 4, 2, 1, 1]
		global.obs_id = [3, 3, 0, 3, 2, 4, 4, 4, 1, 3, 0, 3, 0, 0, 0, 4, 1, 4, 4, 3, 2, 1, 2, 0, 2, 2, 1, 2, 1, 1]
		global.obs_pos = [3, 3, 2, 2, 0, 2, 1, 1, 1, 2, 4, 2, 4, 1, 0, 0, 0, 1, 4, 4, 3, 3, 4, 0, 2, 3, 4, 0, 3, 1]
	