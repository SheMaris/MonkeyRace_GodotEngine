extends RigidBody2D

func _ready():
	connect("body_enter", self, "_on_body_enter")

func _on_body_enter(other_body):
	print(other_body.get_name())
	if(other_body.get_name()=="arbol_suelo"):
		print("suelo")
		queue_free()
	if(other_body.get_name()=="arbol_izquierda"):
		print("pared izquierda")
		queue_free()
	if(other_body.get_name()=="arbol_derecha"):
		print("pared derecha")
		queue_free()
	if(other_body.get_name()=="monoMarron"):
		queue_free()
	if(other_body.get_name()=="platanin"):
		queue_free()