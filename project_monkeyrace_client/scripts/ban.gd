extends Area2D

# Banana Script functions:
# - Update banana position according to the background
# - When a Player collide banana disappear and score increase 

var _bg = null
var _speed_factor = 1.5

func _ready():
	_bg = get_node("../Sprite_bg_vines")
	set_process(true)

func _process(delta):
	set_pos(get_pos() + Vector2(0, _bg.speed * _speed_factor * delta ))
	if (get_pos().y>450):
		queue_free()

func _on_Area2D_body_enter( body ):
	if "Player" in body.get_name():
		body.plus_score()
		queue_free()