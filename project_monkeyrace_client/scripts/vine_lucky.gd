extends Area2D

var descend=true
var _bg = null
var _speed_factor = 1.5

func _ready():
	_bg = get_node("../Sprite_bg_vines")
	set_process(true)

func _process(delta):
	if(get_pos().y>=-700 and get_pos().y<-130 and descend):
		set_pos(get_pos() + Vector2(0, _bg.speed * _speed_factor * delta ))
	elif(get_pos().y>=-130 and descend):
		descend=false
	elif (get_pos().y>-750 and not descend):
		set_pos(get_pos() + Vector2(0, (_bg.speed * _speed_factor * delta)*-1 ))
	elif (get_pos().y<=-750 and not descend):
		queue_free()

func _on_Area2D_body_enter( body ):
	if "Player" in body.get_name():
		print(get_pos().x,get_pos().y,"vine lucky collision with ",body.get_pos().x,body.get_pos().y,body.get_name())
		if (body.has_method("lucked")):
			body.lucked()