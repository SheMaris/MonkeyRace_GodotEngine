extends Node


export(int) var arr = []
var music
var monkey=global.Player
var marcar_marron
var marcar_blanco

func _ready():
	OS.set_window_size(Vector2(480,800))
	marcar_marron=get_node("marcar_marron")
	marcar_blanco=get_node("marcar_blanco")
	music=get_node("music")
	music.set_volume(info.getVolume())
	music.play()
	if(monkey=="Player"):
		marcar_blanco.hide()
	else:
		marcar_marron.hide()
	set_process(true)
	global.conn.connect("connected",self,"_on_connected")
	global.conn.connect("message",self,"_on_receive")
	global.conn.connect("left",self,"_on_left")
	global.conn.connect("start",self,"_on_start")
	
func _on_start():
	var packed_scene = load("res://scenes/game.tscn")
	get_tree().change_scene_to(packed_scene)
	
func _process(dt):
	global.conn.is_listening()
		
	if Input.is_action_pressed("ui_accept"):
		global.conn.ping()
		
func _on_connected():
	print("Connected to server")
	
func _on_left(data):
	print(data)
	
func _on_receive(data):
	print("Receive Data: ",data)
	if (data.data.has("playerid")):
		var monkeyname=data.data.playerid
		var monkey = get_node("Players/"+monkeyname)
		monkey.set_pos(Vector2(data.data.x,data.data.y))
		if(data.data.has("b")):
			if (monkeyname=="Player"):
				info.platanos_marron=data.data.b
				var label_marron=get_node("label_marron")
				label_marron.set_text(str(info.platanos_marron))
			else:
				info.platanos_blanco=data.data.b
				var label_blanco=get_node("label_blanco")
				label_blanco.set_text(str(info.platanos_blanco))