extends Area2D

# Bird Script functions:
# - With a random number we specificate if bird flies from right to left or viceversa

var j=0

func _ready():
	if(j==1):
		set_pos(Vector2(-75, get_pos().y))
	else:
		set_pos(Vector2(650, get_pos().y))
	set_process(true)
#215 250
func _process(delta):
	if(j==0):
		set_pos(Vector2(get_pos().x-delta*215, get_pos().y+delta*400))
	else:
		get_node( "Sprite" ).set_flip_h( true )
		set_pos(Vector2(get_pos().x+delta*215, get_pos().y+delta*400))

	if (get_pos().y>450):
		#print(get_pos().y)
		queue_free()

func set_side(side):
	j=side

func _on_bird_body_enter( body ):
	if "Player" in body.get_name():
		#print("collision with "+body.get_name())
		body.minus_score()