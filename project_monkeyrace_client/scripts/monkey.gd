extends RigidBody2D

# Monkey Script functions:
# - Transform inputs events to new position #There's a fail where redimension screen 
# - Save score
# - When body collide (rock) do something

var lucked = false
var penalized=false
var paralized=false
var count=0

var move_left=false
var move_right=false

var last_moviment = null
var monkey_enemy= null
var count_effect = 0

var positions = [0,132, 212, 280, 353, 420,0]
var label_marron

func _ready():
	label_marron=get_node("/root/main/label_marron")
	label_marron.set_text(str(info.platanos_marron))
	monkey_enemy = get_node("../Player1")
	connect("body_enter",self,"_on_body_enter")
	if(global.Player=="Player"):
		set_process(true)
		set_process_input(true)
	else:
		set_process(false)
		set_process_input(false)

func _input(event):
	print("_input")
	if((event.type==InputEvent.MOUSE_BUTTON) and event.is_pressed() and not event.is_echo() and not paralized):
		if(event.pos.x < get_pos().x):
			if(global.Player=="Player1"):
				print()
			else:
				move_left()
				last_moviment="move_left"
		else:
			if(global.Player=="Player1"):
				print()
			else:
				move_right()
				last_moviment="move_right"

	if(event.type==InputEvent.KEY and event.is_pressed() and not event.is_echo() and not paralized):
		if (event.is_action_pressed("mono_marron_izquierda")):
			print("event KEY 2",move_left," ",move_right)
			if(global.Player=="Player1"):
				print()
			else:
				move_left()
				last_moviment="move_left"
				move_left=false
		if (event.is_action_pressed("mono_marron_derecha")):
			if(global.Player=="Player1"):
				print()
			else:
				move_right()
				last_moviment="move_right"
				move_right=false
				
		if(global.Player=="Player"):
			var poscontx=monkey_enemy.get_pos().x
			var posconty=monkey_enemy.get_pos().y
			print("mymonkey = ",get_pos()," enemy =",monkey_enemy.get_pos())
			if(get_pos().x==poscontx and get_pos().y==posconty):
				if (last_moviment=="move_left"):
					monkey_enemy.move_left()
				else:
					monkey_enemy.move_right()
				global.conn.multicast_data({playerid="Player1",x=monkey_enemy.get_pos().x,y=monkey_enemy.get_pos().y})

func _on_body_enter(other):
	print(self.get_name()," is colliding with body ",other.get_name())
	if("rock" in other.get_name()):
		penalized()
	if("stick" in other.get_name()):
		penalized()
	if("Player" in other.get_name()):
		if (last_moviment=="move_right"):
			move_right()
		else:
			move_left()


func _process(delta):
	count+=1
	if(count%20==0):
		synchronise_positions()
	if (paralized or penalized or lucked):
		count_effect+=1
		if (count_effect>=300):
			if(paralized):
				paralized=false
				set_process_input(true)
			if(penalized):
				penalized=false
				set_pos(Vector2(get_pos().x,get_pos().y-150))
			if(lucked):
				lucked=false
				set_pos(Vector2(get_pos().x,get_pos().y+150))
			if(count_effect==300):
				count_effect=0

func move_right():
	var pos_x = get_pos().x
	var index_pos = positions.find(int(pos_x))
	if(global.Player=="Player"):
		if (positions[index_pos+1]!=0):
			set_pos(Vector2(positions[index_pos+1],get_pos().y))
	else:
		if (positions[index_pos+1]!=0):
			set_pos(Vector2(positions[index_pos+1],get_pos().y))
		else:
			set_pos(Vector2(positions[index_pos-1],get_pos().y))


func move_left():
	var pos_x = get_pos().x
	var index_pos = positions.find(int(pos_x))
	if(global.Player=="Player"):
		if (positions[index_pos-1]!=0):
			set_pos(Vector2(positions[index_pos-1],get_pos().y))
	else:
		if (positions[index_pos-1]!=0):
			set_pos(Vector2(positions[index_pos-1],get_pos().y))
		else:
			set_pos(Vector2(positions[index_pos+1],get_pos().y))

func synchronise_positions():
	if(global.Player=="Player"):
		global.conn.multicast_data({playerid="Player",x=get_pos().x,y=get_pos().y,b=info.platanos_marron})
		###global.conn.multicast_data({playerid="Player1",method="synchronise_position",positionx=monkey_enemy.get_pos().x,positiony=monkey_enemy.get_pos().y})
	#print ("mi mono posicion: ", get_pos())

func plus_score():
	info.platanos_marron+=1
	label_marron.set_text(str(info.platanos_marron))

func minus_score():
	if(info.platanos_marron>0):
		info.platanos_marron-=1
		label_marron.set_text(str(info.platanos_marron))

func paralized():
	paralized=true
	set_process_input(false)

func penalized():
	get_node("../../Camera2D").shake(0.1,500,10)
	penalized = true
	if(get_pos().y<=349):
		set_pos(Vector2(get_pos().x,get_pos().y+150))

func lucked():
	lucked=true
	if(get_pos().y>=51):
		set_pos(Vector2(get_pos().x,get_pos().y-150))
